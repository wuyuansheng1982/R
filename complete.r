complete <- function(directory, id=1:332) {
  ## 'directory' is a character vector of length 1 indicating
  ## the location of the CSV files
  
  ## 'pollutant' is a character vector of length 1 indicating
  ## the name of the pollutant for which we will calculate the
  ## mean; either "sulfate" or "nitrate".
  
  ## 'id' is an integer vector indicating the monitor ID numbers
  ## to be used
  
  ## Return the mean of the pollutant across all monitors list
  ## in the 'id' vector (ignoring NA values)
  ## NOTE: Do not round the result!
  ids <- numeric(length(id))
  nobs <- numeric(length(id))
  counter <- 1
  
  for (i in id) {
    if (i< 10) {
      data = read.csv(paste0(directory, "/00", i, ".csv"))
    } else if (i>=10 && i <100) {
      data = read.csv(paste0(directory, "/0", i, ".csv"))
    } else {
      data = read.csv(paste0(directory, "/", i, ".csv"))
    }
    
    ids[counter] = i
    nobs[counter] = sum(complete.cases(data))
    counter = counter + 1
  }
  data.frame(ids, nobs)
}